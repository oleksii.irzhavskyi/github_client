//
//  EditViewController.swift
//  Github_client
//
//  Created by Oleksiy Irzhavsky on 07.12.2020.
//

import UIKit

class EditViewController: UIViewController {
    

    @IBOutlet weak var Image: UIImageView!
    @IBOutlet weak var editNav: UINavigationItem!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var blogText: UITextField!
    @IBOutlet weak var companyText: UITextField!
    @IBOutlet weak var locationText: UITextField!
    @IBOutlet weak var bioText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let URL="https://avatars1.githubusercontent.com/u/47313528?s=88&v=4"
        let yourColor : UIColor = UIColor(red: 0.6275, green: 0, blue: 0.6275, alpha: 1.0)
        Image.layer.borderColor = yourColor.cgColor
        Image.layer.cornerRadius = Image.bounds.width / 2.3
        Image.setImageWith(NSURL(string: URL)! as URL)
        nameText.borderStyle=UITextField.BorderStyle(rawValue: 0)!
        blogText.borderStyle=UITextField.BorderStyle(rawValue: 0)!
        companyText.borderStyle=UITextField.BorderStyle(rawValue: 0)!
        locationText.borderStyle=UITextField.BorderStyle(rawValue: 0)!
        bioText.borderStyle=UITextField.BorderStyle(rawValue: 0)!
    }
}
