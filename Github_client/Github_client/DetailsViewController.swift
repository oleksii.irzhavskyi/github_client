//
//  DetailsViewController.swift
//  Github_client
//
//  Created by Oleksiy Irzhavsky on 06.12.2020.
//

import UIKit
import AFNetworking

class DetailsViewController: UIViewController {
    @IBOutlet weak var githubImage: UIImageView!
    @IBOutlet weak var githubDisplayNameLabel: UILabel!
    @IBOutlet weak var githubNameLabel: UILabel!
    @IBOutlet weak var githubBioLabel: UILabel!
    @IBOutlet weak var githubEmailLabel: UILabel!
    @IBOutlet weak var githubBlogLabel: UILabel!
    @IBOutlet weak var githubCompanyLabel: UILabel!
    @IBOutlet weak var githubLocationLabel: UILabel!

    
    var githubDisplayName = ""
    var githubAvatarURL = ""
    var githubBio = ""
    var githubName = ""
    var githubEmail = ""
    var githubBlog = ""
    var githubCompany = ""
    var githubLocation = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        githubImage.layer.masksToBounds = true
        githubImage.layer.borderWidth = 2
        let yourColor : UIColor = UIColor(red: 0.6275, green: 0, blue: 0.6275, alpha: 1.0)
        githubImage.layer.borderColor = yourColor.cgColor
        githubImage.layer.cornerRadius = githubImage.bounds.width / 2.3
        githubImage.setImageWith(NSURL(string: githubAvatarURL)! as URL)
        githubBioLabel.text = githubBio
        githubNameLabel.text = githubDisplayName
        githubBlogLabel.text = "🌐"+githubBlog
        githubEmailLabel.text = "✉️"+githubEmail
        githubCompanyLabel.text = "🏢"+githubCompany
        githubLocationLabel.text = "🗺️"+githubLocation
        githubDisplayNameLabel.text = githubName
    }
    
}
//extension UIImageView {
//    func load(url: URL) {
//        DispatchQueue.global().async { [weak self] in
//            if let data = try? Data(contentsOf: url) {
//                if let image = UIImage(data: data) {
//                    DispatchQueue.main.async {
//                        self?.image = image
//                    }
//                }
//            }
//        }
//    }
//}
